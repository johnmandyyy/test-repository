import datetime
class Activity:

    def __init__(self):
        pass

    def one(self):

        day_1 = input("Enter Date: yyyy,m,d format").strip().split(",")
        day_2 = input("Enter Date: yyyy,m,d format").strip().split(",")
        if len(day_1) == 3 and len(day_2) == 3:
            try:
                day_1 = datetime.date(int(day_1[0]), int(day_1[1]), int(day_1[2]))
                day_2 = datetime.date(int(day_2[0]), int(day_2[1]), int(day_2[2]))
                if abs((day_2 - day_1).days) < 2:
                    return abs((day_2 - day_1).days), "day"
                else:
                    
                    return abs((day_2 - day_1).days), "days"
            except:
                return self.one()

    def two(self):
        num = input('Enter a number: ')
        try:
            if (int(num) % 2 == 0):
                return "Even"
            else:
                return "Odd"
        except:
            self.two()

    def three(self):
        lists_of_number = []
        i=0
        while i < 4:
            number = input('Enter Number: ')
            try:
                lists_of_number.append(int(number))
            except:
                print("Try again not a number")
                continue
            i = i + 1
        i = 0
        values = []
        for x in lists_of_number:
            values.append(str(((abs(2000 - int(x)) <= 100) or (abs(1000 - int(x)) <= 100))) + " = " + str(lists_of_number[i]))
            i = i + 1

        for x in values:
            print(x)
        return values

    def four(self, amt = '', interest = '', years = ''):
        try:
            if amt == '' and interest == '' and years == '':
                amt = float(input("Enter Amount: "))
                interest = float(input("Enter Interest: "))
                years = float(input("Enter Years: "))

            x = amt*((1+(0.01*interest)) ** years)
            return x

        except e as Exception:
            print(e)
            self.four()

    def five(self):
        char = input('Enter a string: ')

        tmp = char[0]

        for i in range(1,len(char)):

            if(char[0] == char[i] ):
                tmp = tmp + '$'
            else:
                tmp = tmp + char[i]

        return tmp

    def six(self, a, b):

        if (type(a) == str and type(b) == str):
            return str(a) + str(b)
        elif (type(a) == int and type(b) == int):
            return int(a) * int(b)
        elif (type(a) == list and type(b) == list):
            return a + b
        elif (type(a) == dict and type(b) == dict):
            a.update(b)
            return a
        elif (isinstance(a, datetime.date) == True and isinstance(b, datetime.date) == True):
            return abs((a - b).days) * 24 * 60
            


class Pet:

    def __init__(self, dog_name, dog_breed, dog_color):
        self.dog_name = dog_name
        self.dog_breed = dog_breed
        self.dog_color = dog_color

        self.showMessage()
        

    def showMessage(self):
        print("In pet class:", "I bought a", self.dog_color, self.dog_breed,"and I named my dog",self.dog_name)
  


class Overrides(Pet):
    def __init__(self, dog_name, dog_breed, dog_color):
        
        Pet.__init__(self, dog_name, dog_breed, dog_color)
        self.showMessage()
    def showMessage(self):
        print("In Overrides class:", str(self.dog_name).upper())




class Main:
  def __init__(self, message):
    self.message = message

  def showMessage(self):
    print(self.message)

class Sub(Main):
  def __init__(self, message):
    super().__init__(message)


        

A = Activity()
print(A.one())
print(A.two())
A.three()
print(A.four())
print(A.five())

print(A.six("Hello", "John Mandy"))
print(A.six(2, 5))
print(A.six([1,2,3,4], [5,6,7,8]))
print(A.six({1: 'orange', 2: 'potato'}, {3: 'banana', 4: 'saging'}))
print(A.six(datetime.date(2022, 8, 22), datetime.date(2023, 8, 22)))


#S = Pet("Black", "Labrador", "Max")
P = Overrides("Black", "Labrador", "Max")

instance = Sub("Magandang Umaga")

instance.showMessage()



