from django.db import models
from django.conf import settings


class Posts(models.Model):
    user_posted = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    content = models.TextField()

class Interactions(models.Model):
    interacts_to = models.ForeignKey(Posts, on_delete=models.CASCADE)
    interaction = models.TextField()

class Followers(models.Model):
    thisuser = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='account')
    follows = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='follows')
    
