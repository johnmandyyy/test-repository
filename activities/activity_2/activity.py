class ActivityTwo:

    def __init__(self):
        pass


    def one(self):
      return 'Twinkle, twinkle, little star,\n How I wonder what you are!\n\tUp above the world so high,\n\tLike a diamond in the sky.\nTwinkle, twinkle, little star,\n How I wonder what you are'

 
    def two(self):
        import sys
        return sys.version


    def three(self):
        count = input('Enter String: ')
        return len(count)


    def four(self, name = ''):
        if name == '':
            name = input("Enter Name:")
        try:
            age = int(input("Enter Age:"))
            if age < 0:
                print("Not a valid age.")
                return self.four(name)
        except:
            print("Not a valid age.")
            return self.four(name)
        else:
           
            return name, age

    def five(self):
        from datetime import datetime as dt
        return dt.now()



    def six(self):
        inputs = input('Enter numbers with , ex: 1,2,3,4\n').strip().split(",")
        
        for x in inputs:
            try:
                temp = int(x)
            except:
                print("Integers only! Try again")
                return self.six()
        return list(inputs), tuple(inputs)

    def seven(self):
        color_list = ["Red","Green","White" ,"Black"]
        return str(color_list[0]), str(color_list[len(color_list) - 1])

    def eight(self):
        list_a = input('Element 1: Enter any contents and seperate with , ex: John, 22: ').strip().split(",")
        list_b = input('Element 2: Enter any contents and seperate with , ex: John, 22: ').strip().split(",")
        a = list_a + list_b
        emptyString = ""
        for x in a:
            emptyString = emptyString + str(x) + " "

        return "Type of returning value: ", type(emptyString), emptyString

    def nine(self):
        
        try:
            n = int(input('Enter Number: '))
            i = 1
            j = 1
            #n = 5
            finalStr = ""
            finalSum = 0
            while i < 4:
                while j <= i:
                    temp = str(n) * j
                    j = j + 1
                finalStr = temp
                finalSum = finalSum + int(finalStr)
                j = 1
                i = i + 1

            return finalSum
            
        except:
            return self.nine()


        
        
        
        
            


A = ActivityTwo()

print(A.one())
print(A.two())
print(A.three())
n, a = A.four()
print("Your name is:", n, "Age:", a)
print(A.five())
l, tups = A.six()
print(l, tups)
print(A.seven())
print(A.eight())
print(A.nine())

